package com.stimicode.ostrichmann.partychat.manager;


import com.stimicode.ostrichmann.partychat.main.Global;
import com.stimicode.ostrichmann.partychat.main.Log;
import com.stimicode.ostrichmann.partychat.object.Party;
import com.stimicode.ostrichmann.partychat.util.C;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Derrick on 6/18/2015.
 */
public class MessageManager {

    /* Stores the player name and the hash code of the last message sent to prevent error spamming the player. */
    private static HashMap<String, Integer> lastMessageHashCode = new HashMap<>();

    public static void sendErrorNoRepeat(Object sender, String line) {
        if (sender instanceof Player) {
            Player player = (Player)sender;
            Integer hashcode = lastMessageHashCode.get(player.getName());
            if (hashcode != null && hashcode == line.hashCode()) {
                return;
            }
            lastMessageHashCode.put(player.getName(), line.hashCode());
        }
        send(sender, C.Red + line);
    }

    public static void sendError(Object sender, String line) {
        send(sender, C.Red + line);
    }

    /*
     * Sends message to playerName(if online) AND console.
     */
    @SuppressWarnings("deprecation")
    public static void console(String playerName, String line) {
        try {
            Player player = Global.getPlayer(playerName);
            send(player, line);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.info(line);
    }

    public static void send(Object sender, String line) {
        if ((sender instanceof Player)) {
            ((Player) sender).sendMessage(line);
        } else if (sender instanceof CommandSender) {
            ((CommandSender) sender).sendMessage(line);
        }
    }

    public static void send(Object sender, String[] lines) {
        boolean isPlayer = false;
        if (sender instanceof Player)
            isPlayer = true;

        for (String line : lines) {
            if (isPlayer) {
                ((Player) sender).sendMessage(line);
            } else {
                ((CommandSender) sender).sendMessage(line);
            }
        }
    }

    public static String buildTitle(String title) {
        String line =   "-------------------------------------------------------";
        String titleBracket = "[ " + C.Green + C.BOLD + title + C.Aqua + C.BOLD + " ]";

        if (titleBracket.length() > line.length()) {
            return C.Aqua + C.BOLD + "-" + titleBracket + "-";
        }
        int min = (line.length() / 2) - titleBracket.length() / 2;
        int max = (line.length() / 2) + titleBracket.length() / 2;
        String out = C.Aqua + C.BOLD + line.substring(0, Math.max(0, min));
        out += titleBracket + line.substring(max);
        return out;
    }

    public static String buildSmallTitle(String title) {
        String line =   C.Aqua + C.BOLD + "------------------------------";
        String titleBracket = "[ "+title+" ]";
        int min = (line.length() / 2) - titleBracket.length() / 2;
        int max = (line.length() / 2) + titleBracket.length() / 2;
        String out = C.Aqua + C.BOLD + line.substring(0, Math.max(0, min));
        out += titleBracket + line.substring(max);
        return out;
    }

    public static void sendSubHeading(CommandSender sender, String title) {
        send(sender, buildSmallTitle(title));
    }

    public static void sendHeading(CommandSender sender, String title) {
        send(sender, buildTitle(title));
    }

    public static void sendSuccess(CommandSender sender, String message) {
        send(sender, C.Green + message);
    }

    public static void send(CommandSender sender, List<String> outs) {
        for (String str : outs) {
            send(sender, str);
        }
    }

    public static void sendChat(Player sender, String format, String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String msg = String.format(format, sender.getName(), message);
            player.sendMessage(msg);
        }
    }

    public static void sendAll(String str) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(str);
        }
    }

    public static void sendPartyChat(Party party, Player player, String format, String message) {
        if (party == null) {
            sendError(player, "You are not in a party, nobody hears you. Type /partychat to return to normal chat.");
            return;
        }

        String msg = C.Gold + "[Party] " + C.White + String.format(format, player.getName(), message);
        for (String string : party.getMembers()) {
            Player player1 = Global.getPlayer(string);
            send(player1, msg);
        }
        send(party.getLeader(), msg);
    }

    public static void sendParty(Party party, String message) {
        for (String string : party.members) {
            send(Global.getPlayer(string), C.Gold + "[Party] " + ChatColor.RESET + message);
        }
        send(party.getLeader(), C.Gold + "[Party] " + ChatColor.RESET + message);
    }
}
