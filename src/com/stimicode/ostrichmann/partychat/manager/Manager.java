package com.stimicode.ostrichmann.partychat.manager;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Derrick on 6/21/2015.
 */
public class Manager {

    public static JavaPlugin plugin;

    public static SettingsManager settingsManager;

    public static void init(JavaPlugin plugin) {
        Manager.plugin = plugin;
        settingsManager = new SettingsManager();
        settingsManager.init(plugin);
    }
}
