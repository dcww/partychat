package com.stimicode.ostrichmann.partychat.manager;


import com.stimicode.ostrichmann.partychat.main.Log;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Derrick on 6/17/2015.
 */
public class SettingsManager {

    public JavaPlugin plugin;

    public FileConfiguration config; /* Main Configuration */

    public HashMap<String, FileConfiguration> configs = new HashMap<>();

    public void init (JavaPlugin javaplugin) {
        plugin = javaplugin;
        config = javaplugin.getConfig();

        if (!javaplugin.getDataFolder().exists()) {
            javaplugin.getDataFolder().mkdir();
        }
        try {
            loadConfigurationFiles();
            loadConfigurationObjects();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadConfigurationFiles() throws IOException, InvalidConfigurationException {
    }

    public void loadConfigurationObjects() throws IOException, InvalidConfigurationException {
    }

    public FileConfiguration loadConfigurationFile(String filepath) throws IOException, InvalidConfigurationException {
        File file = new File(plugin.getDataFolder().getPath()+"/"+filepath);
        if (file.exists()) {
            Log.info("Loading Configuration file:" + filepath);

            YamlConfiguration cfg = new YamlConfiguration();
            cfg.load(file);
            return cfg;
        } else {
            Log.warning("Configuration file:" + filepath + " was missing. Attempting to create.");
            file.createNewFile();
            loadConfigurationFile(filepath);
            loadDefaults(filepath);
        }
        return null;
    }

    public void loadDefaults(String filepath) {

    }

    public String getStringBase(String path) throws Exception {
        return getString(plugin.getConfig(), path);
    }

    public Integer getInteger(String path) throws Exception {
        return getInteger(plugin.getConfig(), path);
    }

    public double getDouble(String path) throws Exception {
        return getDouble(plugin.getConfig(), path);
    }

    public boolean getBoolean(String path) throws Exception {
        return getBoolean(plugin.getConfig(), path);
    }

    public Integer getInteger(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration integer "+path);
        }

        int data = cfg.getInt(path);
        return data;
    }

    public String getString(FileConfiguration cfg, String path) throws Exception {
        String data = cfg.getString(path);
        if (data == null) {
            throw new Exception("Could not get configuration string "+path);
        }
        return data;
    }

    public double getDouble(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration double "+path);
        }
        double data = cfg.getDouble(path);
        return data;
    }

    public boolean getBoolean(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration boolean "+path);
        }
        boolean data = cfg.getBoolean(path);
        return data;
    }
    
    public void getLocation(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration location "+path);
        }
        
        Location location = getMetaLocation(cfg, path);
    }

    public void getLocation(FileConfiguration cfg, String path, String subPath) throws Exception {
        String temp = path+"."+subPath;

        if (!cfg.contains(temp)) {
            throw new Exception("Could not get configuration location "+temp);
        }

        Location location = getMetaLocation(cfg, temp);
    }

    public Location getMetaLocation(FileConfiguration cfg, String path){
        World w = Bukkit.getServer().getWorld(cfg.getString(path + ".world"));
        double x = cfg.getDouble(path + ".x");
        double y = cfg.getDouble(path + ".y");
        double z = cfg.getDouble(path + ".z");
        float yaw = (float) cfg.getDouble(path + ".yaw");
        float pitch = (float) cfg.getDouble(path + ".pitch");
        return new Location(w, x, y, z, yaw, pitch);
    }

    public void setLocation(FileConfiguration cfg, String id, Location location) throws Exception {
        cfg.set(id + ".world", location.getWorld().getName());
        cfg.set(id + ".x", location.getX());
        cfg.set(id + ".y", location.getY());
        cfg.set(id + ".z", location.getZ());
        cfg.set(id + ".yaw", location.getYaw());
        cfg.set(id + ".pitch", location.getPitch());
        cfg.save(plugin.getDataFolder().getPath() + "/" + config + ".yml");
    }
}
