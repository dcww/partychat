package com.stimicode.ostrichmann.partychat.util;

import org.bukkit.ChatColor;

/**
 * Created by Derrick on 6/16/2015.
 */
public class C {

    /**
     * Class Name C = Color
     */
    /*
    B = BOLD
    D = Dark
    L = Light
    Color = Color
     */

    public static String BOLD = ChatColor.BOLD+"";
    public static String Red = ChatColor.RED+"";
    public static String D_Red = ChatColor.DARK_RED+"";
    public static String Aqua = ChatColor.AQUA+"";
    public static String D_Aqua = ChatColor.DARK_AQUA+"";
    public static String Blue = ChatColor.BLUE+"";
    public static String D_Blue = ChatColor.DARK_BLUE+"";
    public static String Green = ChatColor.GREEN+"";
    public static String D_Green = ChatColor.DARK_GREEN+"";
    public static String L_Purple = ChatColor.LIGHT_PURPLE+"";
    public static String D_Purple = ChatColor.DARK_PURPLE+"";
    public static String Gold = ChatColor.GOLD+"";
    public static String Yellow = ChatColor.YELLOW+"";
    public static String Gray = ChatColor.GRAY+"";
    public static String D_Gray = ChatColor.DARK_GRAY+"";
    public static String White = ChatColor.WHITE+"";

    public static String B_Red = ChatColor.RED+""+ ChatColor.BOLD;
    public static String B_D_Red = ChatColor.DARK_RED+""+ ChatColor.BOLD;
    public static String B_Aqua = ChatColor.AQUA+""+ ChatColor.BOLD;
    public static String B_D_Aqua = ChatColor.DARK_AQUA+""+ ChatColor.BOLD;
    public static String B_Blue = ChatColor.BLUE+""+ ChatColor.BOLD;
    public static String B_D_Blue = ChatColor.DARK_BLUE+""+ ChatColor.BOLD;
    public static String B_Green = ChatColor.GREEN+""+ ChatColor.BOLD;
    public static String B_D_Green = ChatColor.DARK_GREEN+""+ ChatColor.BOLD;
    public static String B_L_Purple = ChatColor.LIGHT_PURPLE+""+ ChatColor.BOLD;
    public static String B_D_Purple = ChatColor.DARK_PURPLE+""+ ChatColor.BOLD;
    public static String B_Yellow = ChatColor.YELLOW+""+ ChatColor.BOLD;
    public static String B_Gold = ChatColor.GOLD+""+ ChatColor.BOLD;
}
