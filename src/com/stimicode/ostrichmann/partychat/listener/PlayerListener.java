package com.stimicode.ostrichmann.partychat.listener;

import com.stimicode.ostrichmann.partychat.main.Global;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Derrick on 9/20/2015.
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        Global.partyChat.put(player.getName().toLowerCase(), false);
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Global.partyChat.remove(player.getName().toLowerCase());
    }
}
