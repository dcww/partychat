package com.stimicode.ostrichmann.partychat.listener;

import com.stimicode.ostrichmann.partychat.main.Global;
import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import com.stimicode.ostrichmann.partychat.object.Party;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Derrick on 9/19/2015.
 */
public class ChatListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    void onPlayerAsyncChatEvent(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        if (Global.isPartyChat(player)) {
            Party party = Global.getParty(player);
            event.setCancelled(true);
            MessageManager.sendPartyChat(party, player, event.getFormat(), event.getMessage());
        }
    }
}
