package com.stimicode.ostrichmann.partychat.main;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Derrick on 9/19/2015.
 */
public class Log {

    public static JavaPlugin plugin;
    private static Logger cleanupLogger;

    /**
     * Initializes our Custom Logger
     */
    public static void init(JavaPlugin plugin) {
        Log.plugin = plugin;

        cleanupLogger = Logger.getLogger("cleanUp");
        FileHandler fh;

        try {
            fh = new FileHandler("cleanUp.log");
            cleanupLogger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a Heading for a Log Section
     * @param title
     */
    public static void heading(String title) {
        plugin.getLogger().info("========= "+title+" =========");
    }

    /**
     * Generic Logger
     * @param message
     */
    public static void info(String message) {
        plugin.getLogger().info(message);
    }

    /**
     * Debug Logger
     * @param message
     */
    public static void debug(String message) {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        plugin.getLogger().info("[DEBUG] : " + message);
    }

    /**
     * Warning Logger
     * @param message
     */
    public static void warning(String message) {
        plugin.getLogger().info("[WARNING] : " + message);
    }

    public static void error(String message) {
        plugin.getLogger().severe(message);
    }

    public static void adminlog(String name, String message) {
        plugin.getLogger().info("[ADMIN : " + name + " ] " + message);
    }

    public static void cleanupLog(String message) {
        info(message);
        cleanupLogger.info(message);
    }

    public static void exception(Exception e) {
        e.printStackTrace();
    }
}
