package com.stimicode.ostrichmann.partychat.main;

import com.stimicode.ostrichmann.partychat.object.Party;
import com.stimicode.ostrichmann.partychat.response.QuestionResponseInterface;
import com.stimicode.ostrichmann.partychat.threading.PlayerQuestionTask;
import com.stimicode.ostrichmann.partychat.threading.QuestionBaseTask;
import com.stimicode.ostrichmann.partychat.threading.TaskMaster;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Derrick on 9/19/2015.
 */
public class Global {

    public static HashMap<String, Boolean> partyChat = new HashMap<>();
    private static Map<String, QuestionBaseTask> questions = new ConcurrentHashMap<>();

    public static boolean isPartyChat(Player player) {
        return partyChat.get(player.getName().toLowerCase());
    }

    public static void setPartychat(Player player, boolean bool) {
        partyChat.replace(player.getName().toLowerCase(), !(partyChat.get(player.getName().toLowerCase())));
    }

    @Deprecated
    public static Player getPlayer(String _playerName) {
        return Bukkit.getPlayer(_playerName);
    }

    public static Player getPlayer(UUID _playerUUID) {
        return Bukkit.getPlayer(_playerUUID);
    }

    public static Player getPlayerUUID(String _playerUUID) {
        return Bukkit.getPlayer(UUID.fromString(_playerUUID));
    }

    public static boolean hasParty(Player player) {
        Party party = Party.getPartyForPlayer(player);
        if (party == null) {
            return false;
        }
        return true;
    }

    public static Party getParty(Player player) {
        Party party = Party.getPartyForPlayer(player);
        if (party == null) {
            return null;
        }
        return party;
    }

    public static boolean isPartyLeader(Player player) {
        Party party = Party.getPartyForPlayer(player);
        if (party == null) {
            return false;
        }

        if (party.getLeader() == player) {
            return true;
        }
        return false;
    }

    public static void questionPlayer(Player fromPlayer, Player toPlayer, String question, long timeout, QuestionResponseInterface finishedFunction) throws Exception {
        PlayerQuestionTask task = (PlayerQuestionTask) questions.get(toPlayer.getName());
        if (task != null) {
			/* Player already has a question pending. Lets deny this question until it times out
			 * this will allow questions to come in on a pseduo 'first come first serve' and
			 * prevents question spamming.
			 */
            throw new Exception("Player already has a question pending, wait 30 seconds and try again.");
        }

        task = new PlayerQuestionTask(toPlayer, fromPlayer, question, timeout, finishedFunction);
        questions.put(toPlayer.getName(), task);
        TaskMaster.asyncTask("", task, 0);
    }

    public static QuestionBaseTask getQuestionTask(String string) {
        return questions.get(string);
    }

    public static void removeQuestion(String name) {
        questions.remove(name);
    }
}
