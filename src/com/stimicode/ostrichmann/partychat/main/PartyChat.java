package com.stimicode.ostrichmann.partychat.main;

import com.stimicode.ostrichmann.partychat.command.AcceptCommand;
import com.stimicode.ostrichmann.partychat.command.DenyCommand;
import com.stimicode.ostrichmann.partychat.command.PartyChatCommand;
import com.stimicode.ostrichmann.partychat.command.PartyCommand;
import com.stimicode.ostrichmann.partychat.listener.ChatListener;
import com.stimicode.ostrichmann.partychat.listener.PlayerListener;
import com.stimicode.ostrichmann.partychat.manager.Manager;
import com.stimicode.ostrichmann.partychat.util.BukkitUtility;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Derrick on 9/19/2015.
 */
public class PartyChat extends JavaPlugin {

    public JavaPlugin instance;

    @Override
    public void onEnable() {
        initialize();
    }

    @Override
    public void onDisable() {
        //TODO On Disable Code
    }

    public void initialize() {
        setInstance(this);
        this.saveDefaultConfig();
        Log.init(getInstance());
        initCommands();
        initListeners();
        BukkitUtility.initialize(getInstance());
        Manager.init(getInstance());
    }

    public void initCommands() {
        getCommand("accept").setExecutor(new AcceptCommand());
        getCommand("deny").setExecutor(new DenyCommand());
        getCommand("partychat").setExecutor(new PartyChatCommand());
        getCommand("party").setExecutor(new PartyCommand());
    }

    public void initListeners() {
        final PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new ChatListener(), getInstance());
        pluginManager.registerEvents(new PlayerListener(), getInstance());
    }

    public JavaPlugin getInstance() {
        return instance;
    }

    public void setInstance(JavaPlugin instance) {
        this.instance = instance;
    }
}
