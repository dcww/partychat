package com.stimicode.ostrichmann.partychat.response;

import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import com.stimicode.ostrichmann.partychat.object.Party;
import com.stimicode.ostrichmann.partychat.util.C;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/19/2015.
 */
public class JoinPartyResponse implements QuestionResponseInterface {

    public Party party;
    public Player target;
    public Player sender;

    @Override
    public void processResponse(String param) {
        if (param.equalsIgnoreCase("accept")) {
            MessageManager.send(sender, C.Gray + target.getName() + " has accepted our party invitation.");
            party.addMember(target);
            MessageManager.sendParty(party, target.getName() + " has joined the party.");
        } else {
            MessageManager.send(sender, C.Gray + target.getName() + " denied our party invitation.");
        }
    }
}
