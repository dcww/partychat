package com.stimicode.ostrichmann.partychat.response;

/**
 * Created by Derrick on 9/19/2015.
 */
public interface QuestionResponseInterface {
    void processResponse(String param);
}
