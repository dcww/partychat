package com.stimicode.ostrichmann.partychat.command;

import com.stimicode.ostrichmann.partychat.main.Global;
import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/19/2015.
 */
public class PartyChatCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (!(sender instanceof Player)) {
            MessageManager.sendError(sender, "Only a player can execute this command.");
            return false;
        }

        Player player = (Player) sender;

        if (args.length == 0) {
            Global.setPartychat(player, !(Global.isPartyChat(player)));
            MessageManager.sendSuccess(player, "Party chat mode set to " + Global.isPartyChat(player) + ".");
            return true;
        }
        return false;
    }
}
