package com.stimicode.ostrichmann.partychat.command;

import com.stimicode.ostrichmann.partychat.main.Global;
import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import com.stimicode.ostrichmann.partychat.threading.PlayerQuestionTask;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/19/2015.
 */
public class DenyCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

        if (!(sender instanceof Player)) {
            MessageManager.sendError(sender, "Only a player can execute this command.");
            return false;
        }

        Player player = (Player) sender;

        PlayerQuestionTask task = (PlayerQuestionTask) Global.getQuestionTask(player.getName());
        if (task != null) {
            synchronized (task) {
                task.setResponse("deny");
                task.notifyAll();
            }
            return true;
        }

        MessageManager.sendError(sender, "No question to respond to.");
        return false;
    }
}
