package com.stimicode.ostrichmann.partychat.command;

import com.stimicode.ostrichmann.partychat.main.Global;
import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import com.stimicode.ostrichmann.partychat.object.Party;
import com.stimicode.ostrichmann.partychat.response.JoinPartyResponse;
import com.stimicode.ostrichmann.partychat.util.C;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 9/19/2015.
 */
public class PartyCommand extends CommandBase {
    
    @Override
    public void init() {
        command = "/party";
        displayName = "Party";

        commands.put("info", "Lists information about the current party you're in.");
        commands.put("create", "Creates a party for you.");
        commands.put("leave", "Leaves your current party.");
        commands.put("disband", "Disbands your current party. You must be the party leader to do this.");
        commands.put("add", "[player] Add a player to your party.");
        commands.put("remove", "[player] removes a player from your party.");
    }

    public void printPartyInfo(Party party) {
        MessageManager.sendHeading(sender, "Party " + party.getName());
        MessageManager.send(sender, C.Green + "Leader: " + C.Green + party.getLeader().getName());
        MessageManager.send(sender, C.Green + "Members: " + C.Green + party.getMemberListSaveString());
    }

    public void info_cmd() throws Exception {
        Player player = getPlayer();

        if (!(Party.hasParty(player))) {
            throw new Exception("You're not currently part of a party.");
        }

        Party party = Party.getPartyForPlayer(player);
        printPartyInfo(party);
    }

    public void create_cmd() throws Exception {
        Player player = getPlayer();

        if (Party.hasParty(player)) {
            throw new Exception("You can only be on one party at time. Leave your current party first.");
        }

        Party.createParty(player.getName().toLowerCase(), player);
        MessageManager.sendSuccess(sender, "Party Successfully Created.");
    }

    public void leave_cmd() throws Exception {
        Player player = getPlayer();

        if (!(Party.hasParty(player))) {
            throw new Exception("You're not currently part of a party.");
        }

        if (Party.getPartyForPlayer(player).getLeader() == player) {
            throw new Exception("Leaders cannot leave their own party. They must change the leader or disband the party first.");
        }

        Party party = Party.getPartyForPlayer(player);

        party.removeMember(player);
        MessageManager.sendSuccess(sender, "Left Party " + party.getName());
        MessageManager.sendParty(party, player.getName() + " has left the party.");
    }

    public void disband_cmd() throws Exception {
        Player player = getPlayer();

        if (!(Party.hasParty(player))) {
            throw new Exception("You're not currently part of a party.");
        }

        String partyName = Party.getPartyForPlayer(player).getName();
        Party.deleteParty(partyName);
        MessageManager.sendSuccess(sender, "Disbanded party: " + partyName);
    }

    public void add_cmd() throws Exception {
        Player player = getPlayer();
        Player member = getNamedAccount(1);

        if (!(Party.hasParty(player))) {
            throw new Exception("You're not currently part of a party.");
        }

        if (Party.hasParty(member)) {
            throw new Exception(member.getName()+" is already in a party.");
        }

        try {
            Party party = Party.getPartyForPlayer(player);
            System.out.println(party.getName());
            JoinPartyResponse join = new JoinPartyResponse();
            join.party = party;
            join.target = member;
            join.sender = (Player)sender;

            Global.questionPlayer(player, member, "Would you like to join the party " + party.getName() + "?", 30000, join);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        MessageManager.sendSuccess(sender, "Sent invitation to " + member.getName());
    }

    public void remove_cmd() throws Exception {
        Player player = getPlayer();

        if (!(Party.hasParty(player))) {
            throw new Exception("You're not currently part of a party.");
        }

        if (args.length < 1) {
            throw new Exception("Please enter a player name!");
        }

        Player member = getNamedAccount(1);
        if (member == null) {
            throw new Exception("No player by the given name.");
        }
        Party party = Party.getPartyForPlayer(player);

        if (!(Global.isPartyLeader(player))) {
            throw new Exception("You must have a party and be it's leader to add members to your party.");
        }

        party.removeMember(member);
        MessageManager.sendSuccess(sender, "Removed Party Member " + member.getName());
        MessageManager.sendParty(Party.getPartyForPlayer(player), member.getName() + " has been removed from the party.");

    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {

        if (!(getPlayer().hasPermission("partychat.normal"))) {
            throw new Exception("You do not have permission to run this command.");
        }
    }
}
