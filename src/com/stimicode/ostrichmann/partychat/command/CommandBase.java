package com.stimicode.ostrichmann.partychat.command;


import com.stimicode.ostrichmann.partychat.main.Log;
import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import com.stimicode.ostrichmann.partychat.util.C;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Derrick on 6/18/2015.
 */
public abstract class CommandBase implements CommandExecutor {

    private static final int MATCH_LIMIT = 5;

    protected HashMap<String, String> commands = new HashMap<>();

    private HashMap<String, Method> extraCommands = new HashMap<>();

    protected String[] args;
    protected CommandSender sender;

    protected String command = "FIXME";
    protected String displayName = "FIXME";
    protected boolean sendUnknownToDefault = false;

    public abstract void init();

    /* Called when no arguments are passed. */
    public abstract void doDefaultAction() throws Exception;

    /* Called on syntax error. */
    public abstract void showHelp();

    /* Called before command is executed to check permissions. */
    public abstract void permissionCheck() throws Exception;

    public void addCommand(String commandName, Method method) {
        this.extraCommands.put(commandName, method);
    }

    public void removeCommand(String commandName) {
        this.extraCommands.remove(commandName);
    }

    public Method getCommand(String commandName) {
        return this.extraCommands.get(commandName);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        init();

        this.args = args;
        this.sender = sender;

        try {
            permissionCheck();
        } catch (Exception e1) {
            MessageManager.sendError(sender, e1.getMessage());
            return false;
        }

        doLogging();

        if (args.length == 0) {
            try {
                doDefaultAction();
            } catch (Exception e) {
                MessageManager.sendError(sender, e.getMessage());
            }
            return false;
        }

        if (args[0].equalsIgnoreCase("help")) {
            showHelp();
            return true;
        }

        for (String c : commands.keySet()) {
            if (c.equalsIgnoreCase(args[0])) {
                try {
                    Method method = this.getClass().getMethod(args[0].toLowerCase() + "_cmd");
                    try {
                        method.invoke(this);
                        return true;
                    } catch (IllegalAccessException | IllegalArgumentException e) {
                        e.printStackTrace();
                        MessageManager.sendError(sender, "Internal Command Error.");
                    } catch (InvocationTargetException e) {
                        if (e.getCause() instanceof Exception) {
                            //Log.error("Test1");
                            //e.printStackTrace();
                            //e.getCause().printStackTrace();
                            MessageManager.sendError(sender, e.getCause().getMessage());
                        } else {
                            MessageManager.sendError(sender, "Internal Command Error.");
                            e.getCause().printStackTrace();
                        }
                    }
                } catch (NoSuchMethodException e) {
                    if (sendUnknownToDefault) {
                        try {
                            Log.error("Test2");
                            doDefaultAction();
                        } catch (Exception e1) {
                            Log.error("Test3");
                            MessageManager.sendError(sender, e.getMessage());
                        }
                        return false;
                    }
                    MessageManager.sendError(sender, "Unknown method " + args[0]);
                }
                return true;
            }
        }

        if (sendUnknownToDefault) {
            try {
                Log.error("Test4");
                doDefaultAction();
            } catch (Exception e) {
                Log.error("Test5");
                MessageManager.sendError(sender, e.getMessage());
            }
            return false;
        }
        MessageManager.sendError(sender, "Unknown command " + args[0]);
        return false;
    }


    public void doLogging() {
    }

    public void showBasicHelp() {
        MessageManager.sendHeading(sender, displayName + " Command Help");
        for (String c : commands.keySet()) {
            String info = commands.get(c);
            info = info.replace("[", C.Yellow + C.BOLD + "[");
            info = info.replace("]", "]" + C.Gray);
            info = info.replace("(", C.Yellow + C.BOLD + "(");
            info = info.replace(")", ")" + C.Gray);
            MessageManager.send(sender, C.Gold + C.BOLD + command + " " + c + C.Gray + " " + info);
        }
    }

    public Player getPlayer() throws Exception {
        if (sender instanceof Player) {
            return (Player) sender;
        }
        throw new Exception("Only players can do this.");
    }

    protected String[] stripArgs(String[] someArgs, int amount) {
        if (amount >= someArgs.length) {
            return new String[0];
        }

        String[] argsLeft = new String[someArgs.length - amount];
        for (int i = 0; i < argsLeft.length; i++) {
            argsLeft[i] = someArgs[i + amount];
        }
        return argsLeft;
    }

    protected String combineArgs(String[] someArgs) {
        String combined = "";
        for (String str : someArgs) {
            combined += str + " ";
        }
        combined = combined.trim();
        return combined;
    }

    protected Double getNamedDouble(int index) throws Exception {
        if (args.length < (index + 1)) {
            throw new Exception("Enter a number.");
        }

        try {
            Double number = Double.valueOf(args[index]);
            return number;
        } catch (NumberFormatException e) {
            throw new Exception(args[index] + " is not a number.");
        }

    }

    protected Integer getNamedInteger(int index) throws Exception {
        if (args.length < (index + 1)) {
            throw new Exception("Enter a number.");
        }
        try {
            Integer number = Integer.valueOf(args[index]);
            return number;
        } catch (NumberFormatException e) {
            throw new Exception(args[index] + " is not whole a number.");
        }
    }

    protected Player getNamedAccount(int index) throws Exception {
        if (args.length < (index+1)) {
            throw new Exception("Enter a player name.");
        }

        String name = args[index].toLowerCase();
        name = name.replace("%", "(\\w*)");

        ArrayList<Player> potentialMatches = new ArrayList<>();
        for (Player player : Bukkit.getOnlinePlayers()) {
            String str = player.getName().toLowerCase();
            try {
                if (str.matches(name)) {
                    potentialMatches.add(player);
                }
            } catch (Exception e) {
                throw new Exception("Invalid pattern.");
            }

            if (potentialMatches.size() > MATCH_LIMIT) {
                throw new Exception("Too many potential matches. Refine your search.");
            }
        }

        if (potentialMatches.size() == 0) {
            throw new Exception("No players matching that name.");
        }

        if (potentialMatches.size() != 1) {
            MessageManager.send(sender, C.L_Purple+ ChatColor.UNDERLINE+"Potential Matches");
            MessageManager.send(sender, " ");
            String out = "";
            for (Player players : potentialMatches) {
                out += players.getName()+", ";
            }

            MessageManager.send(sender, C.Blue+ChatColor.ITALIC+out);
            throw new Exception("More than one player match, please clarify.");
        }
        return potentialMatches.get(0);
    }

    public String getNamedString(int index, String message) throws Exception {
        if (args.length < (index + 1)) {
            throw new Exception(message);
        }
        return args[index];
    }

    protected OfflinePlayer getNamedOfflinePlayer(int index) throws Exception {
        if (args.length < (index + 1)) {
            throw new Exception("Enter a player name");
        }
        OfflinePlayer offplayer = Bukkit.getOfflinePlayer(args[index]);
        if (offplayer == null) {
            throw new Exception("No player named:" + args[index]);
        }
        return offplayer;
    }

    public String makeInfoString(HashMap<String, String> kvs, String lowColor, String highColor) {
        String out = "";
        for (String key : kvs.keySet()) {
            out += lowColor + key + ": " + highColor + kvs.get(key) + " ";
        }
        return out;
    }
}