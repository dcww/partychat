package com.stimicode.ostrichmann.partychat.object;

import com.stimicode.ostrichmann.partychat.main.Global;
import com.stimicode.ostrichmann.partychat.manager.MessageManager;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by Derrick on 9/19/2015.
 */
public class Party {

    private String partyName;
    private String leaderName;
    public ArrayList<String> members = new ArrayList<>();
    public static HashMap<String, Party> parties = new HashMap<>();

    public Party(Player player) {
        this.setName(player.getName().toLowerCase());
        this.setLeader(player.getName().toLowerCase());
    }

    public void setName(String name) {
        this.partyName = name;
    }

    public String getName() {
        return this.partyName;
    }

    public void setLeader(String name) {
        this.leaderName = name;
    }

    public Player getLeader() {
        return Global.getPlayer(leaderName);
    }

    public void addMember(Player player) {
        members.add(player.getName().toLowerCase());
    }

    public void removeMember(Player player) {
        members.remove(player.getName().toLowerCase());
    }

    public boolean containsMember(Player player) {
        return members.contains(player.getName().toLowerCase());
    }

    public Collection<String> getMembers() {
        return this.members;
    }

    public String getMemberListSaveString() {
        String out = "";
        for (String string : members) {
            out += string + ", ";
        }
        return out;
    }

    public static boolean isInParty(String partyName, Player player) throws Exception {
        Party party = parties.get(partyName);
        if (party == null) {
            throw new Exception("No party with the name " + partyName + "!");
        }
        return party.containsMember(player);
    }

    public static Party getPartyForPlayer(Player player) {
        for (Party party : parties.values()) {
            if (party.containsMember(player)) {
                return party;
            } else if (party.getLeader() == player) {
                return party;
            }
        }
        return null;
    }

    public static void createParty(String partyName, Player leader) throws Exception {
        if (parties.containsKey(partyName)) {
            throw new Exception("A party with that name already exists.");
        }

        Party party = new Party(leader);
        parties.put(party.getName(), party);
        MessageManager.sendSuccess(leader, "Created a new party!");
    }

    public static boolean hasParty(Player player) {
        for (Party party : parties.values()) {
            if (party.containsMember(player) || party.getLeader() == player) {
                return true;
            }
        }
        return false;
    }

    public static void deleteParty(String partyName) {
        Party party = parties.get(partyName);
        Collection<String> temp = party.getMembers();
        temp.remove(party.getLeader().getName().toLowerCase());
        for (String playerName : temp) {
            Player player = Global.getPlayer(playerName);
            MessageManager.sendError(player, party.leaderName + " has disbanded the party!");
        }
        parties.remove(partyName);
    }
}
